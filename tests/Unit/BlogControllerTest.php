<?php

namespace Tests\Feature;

use App\Models\Blog;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BlogControllerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->make(['id' => 1]);
        $this->be($this->user);
    }

    /**
     * Tests blog store
     *
     * @return void
     */
    public function testBlogStore()
    {
        $this->followingRedirects()
             ->post('/blogs', [
                 'title' => $this->faker->sentence(),
                 'content' => $this->faker->paragraph(mt_rand(1, 4)),
                 'user_id' => $this->user->id
             ])
            ->assertStatus(200);
    }

    /**
     * Tests blog store validation fail
     *
     * @return void
     */
    public function testBlogStoreValidationFail()
    {
        $this->post('/blogs', [
                'content' => $this->faker->paragraph(mt_rand(1, 4)),
                'user_id' => $this->user->id
            ])
            ->assertStatus(302);
    }

    /**
     * Tests blog deletion
     *
     * @return void
     */
    public function testDestroy()
    {
        $this->post('/blogs', [
                'title' => $this->faker->sentence(),
                'content' => $this->faker->paragraph(mt_rand(1, 4)),
                'user_id' => $this->user->id
            ]);

        $blog = Blog::first();

        $this->delete('/blogs/' . $blog->id)
            ->assertJson(['success' => true])
            ->assertStatus(200);
    }
}
