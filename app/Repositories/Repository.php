<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Repository implements RepositoryInterface
{
    protected $eloquentModel;

    public function __construct(Model $model)
    {
        $this->eloquentModel = $model;
    }

    public function all(): Collection
    {
        return $this->eloquentModel->get();
    }
}