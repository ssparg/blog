<?php

namespace App\Repositories;

use App\Models\Blog;
use Illuminate\Support\Collection;

/**
 * Class BlogRepository
 * We can add caching to this and invalidate it on updates/creates and deletes
 * @package App\Repositories
 */
class BlogRepository extends Repository implements BlogRepositoryInterface
{
    protected $blog;

    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
    }

    public function all(): Collection
    {
        return $this->blog->with('user')->orderBy('created_at', 'DESC')->get();
    }

    public function create(array $blog): Blog
    {
        return $this->blog->create($blog);
    }

    public function update(Blog $blog, array $data): bool
    {
        return $blog->update($data);
    }

    public function destroy(Blog $blog): bool
    {
        return $blog->delete();
    }
}
