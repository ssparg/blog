<?php

namespace App\Repositories;

use App\Models\Blog;
use Illuminate\Support\Collection;

interface BlogRepositoryInterface
{
    public function all(): Collection;

    public function create(array $blog): Blog;

    public function update(Blog $blog, array $data): bool;

    public function destroy(Blog $blog): bool;
}