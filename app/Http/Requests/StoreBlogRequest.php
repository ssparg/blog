<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class StoreBlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:100',
            'content' => 'required|min:20'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Please enter a blog title.',
            'title.max' => 'Please enter a blog title no longer then 100 characters.',
            'content.required' => 'Please enter blog content.',
            'content.min' => 'Please enter blog content no less then 20 characters.',
        ];
    }
}
