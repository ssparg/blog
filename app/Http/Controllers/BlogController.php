<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBlogRequest;
use App\Models\Blog;
use App\Repositories\BlogRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{

    private $blogRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BlogRepositoryInterface $blogRepository)
    {
        $this->middleware('auth');

        $this->blogRepository = $blogRepository;
    }

    public function index()
    {
        return view('blogs.index');
    }

    public function create()
    {
        return view('blogs.create');
    }

    public function edit(Blog $blog)
    {
        return view('blogs.edit', compact('blog'));
    }

    public function store(StoreBlogRequest $request)
    {
        $data = [
            'title' => $request->get('title'),
            'content' => $request->get('content'),
            'user_id' => Auth::id()
        ];
        $this->blogRepository->create($data);

        return redirect()->route('blogs.index');
    }

    public function update(StoreBlogRequest $request, Blog $blog)
    {
        $this->blogRepository->update($blog, $request->only(['title', 'content']));
        return redirect()->route('blogs.index');
    }

    public function rate(Request $request, Blog $blog)
    {
        $this->blogRepository->update($blog, $request->only(['rating']));
        return response()->json(['success' => true]);
    }

    public function destroy(Blog $blog)
    {
        $this->blogRepository->destroy($blog);
        return response()->json(['success' => true]);
    }

    public function getBlogs()
    {
        $blogs = $this->blogRepository->all();

        return response()->json(compact('blogs'));
    }
}
