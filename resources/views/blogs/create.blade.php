@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mb-3">
        <div class="col">
            <form action="{{ route('blogs.store') }}" method="post">
                @csrf
                <div class="mb-3">
                    <input class="form-control" type="text" name="title" placeholder="Blog Title" value="{{ old('title') }}" required>
                    <small class="form-text text-danger">{{ $errors->first('title') }}</small>
                </div>
                <div class="mb-3">
                <textarea class="form-control" name="content" rows="3" placeholder="Blog Content" required>{{ old('content') }}</textarea>
                    <small class="form-text text-danger">{{ $errors->first('content') }}</small>
                </div>
                <div class="mb-3">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
