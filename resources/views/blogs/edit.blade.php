@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mb-3">
        <div class="col">
            <form name="edit-blog" action="{{ route('blogs.update', ['blog' => $blog->id]) }}" method="POST">
                @csrf
                @method('PATCH')
                <input type="hidden" name="rating">
                <div class="mb-3">
                    <input class="form-control" type="text" name="title" placeholder="Blog Title" value="{{ $blog->title }}" required>
                    <small class="form-text text-danger">{{ $errors->first('title') }}</small>
                </div>
                <div class="mb-3">
                <textarea class="form-control" name="content" rows="3" placeholder="Blog Content" required>{{ $blog->content }}</textarea>
                    <small class="form-text text-danger">{{ $errors->first('content') }}</small>
                </div>
                <div class="mb-3">
                    <button class="btn btn-primary" type="submit">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection